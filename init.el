;;; init.el --- Configuration of Emacs
;;
;; (c) 2000 - 2020 Arjen Wiersma
;;

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(require 'org)
(require 'ob-tangle)

;; Bootstrap straight as package manager
;;(defvar bootstrap-version)
(let ((bootstrap-file
      (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
     (bootstrap-version 5))
 (unless (file-exists-p bootstrap-file)
   (with-current-buffer
       (url-retrieve-synchronously
        "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
        'silent 'inhibit-cookies)
     (goto-char (point-max))
     (eval-print-last-sexp)))
 (load bootstrap-file nil 'nomessage))

;; Load the literal configuration
(setq init-dir (file-name-directory (or load-file-name (buffer-file-name))))
(org-babel-load-file (expand-file-name "loader.org" init-dir))

;;; This stuff is autogenerated

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("835868dcd17131ba8b9619d14c67c127aa18b90a82438c8613586331129dda63" "0edb121fdd0d3b18d527f64d3e2b57725acb152187eea9826d921736bd6e409e" "1ed5c8b7478d505a358f578c00b58b430dde379b856fbcb60ed8d345fc95594e" default))
 '(org-agenda-files
   '("/home/arjen/stack/Notebook/notes.org" "/home/arjen/stack/Notebook/Bookmarks.org" "/home/arjen/stack/Notebook/Canada.org" "/home/arjen/stack/Notebook/NOVI-medewerker.org" "/home/arjen/stack/Notebook/_Goals.org" "/home/arjen/stack/Notebook/boodschappen.org" "/home/arjen/stack/Notebook/gtd.org" "/home/arjen/stack/Notebook/health.org" "/home/arjen/stack/Notebook/htb.org" "/home/arjen/stack/Notebook/inbox.org" "/home/arjen/stack/Notebook/java.org" "/home/arjen/stack/Notebook/jelle.org" "/home/arjen/stack/Notebook/kadoideeen.org" "/home/arjen/stack/Notebook/recepten.org" "/home/arjen/stack/Notebook/tickler.org"))
 '(org-roam-directory "/home/arjen/stack/roam/" nil nil "Customized with use-package org-roam")
 '(package-selected-packages
   '(deft org-roam olivetti-mode olivetti writeroom-mode csharp-mode visual-fill-column lua-mode ox-hugo theme-changer modus-vivendi-theme modus-operandi-theme org-ref ob-restclient typescript expand-region forge highlight-indent-guides hl-todo angular-mode yaml-mode lsp-ivy which-key web-mode use-package undo-tree typescript-mode tree-mode spaceline-all-the-icons solaire-mode smex restclient rainbow-mode rainbow-delimiters racer paredit-everywhere ox-reveal org-present org-plus-contrib org-journal org-bullets mu4e-maildirs-extension moody minions magit-gitflow lsp-ui lsp-java langtool java-snippets ivy-hydra htmlize html-to-hiccup highlight-parentheses guru-mode gotest go-snippets go-guru go-errcheck go-eldoc git-timemachine git-gutter flycheck-rust flycheck-golangci-lint exec-path-from-shell emmet-mode doom-themes doom-modeline dockerfile-mode diminish dap-mode counsel-projectile company-lsp company-go command-log-mode clojure-snippets clj-refactor cargo bm arjen-grey-theme all-the-icons-ivy ace-jump-mode))
 '(safe-local-variable-values
   '((TeX-auto-save . t)
     (TeX-parse-self . t)
     (org-attach-id-dir)
     (org-ref-default-bibliography "../meta/my-biblio.bib")
     (org-ref-default-bibliography quote
                                   ("../meta/my-biblio.bib"))
     (org-ref-default-bibliography . "../meta/my-biblio.bib")
     (reftex-default-bibliography . "../meta/my-biblio.bib")))
 '(warning-suppress-types '((comp) (comp) (:warning))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'narrow-to-region 'disabled nil)
